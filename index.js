'use strict';

const process = require('process');
const util = require('util');
const fs = require('fs');
const path = require('path');
const childproc = require('child_process');
const needle = require('needle');
const minimist = require('minimist');

process.on('unhandledRejection', e => {
	throw e;
});

const config = minimist(process.argv.slice(2), {
	default: {
		maxlen: 1500,
		fmt: '**%P** by %A Release **%R**:%n%C',
		datadir: path.join(process.cwd(), 'data'),
		repodir: path.join(process.cwd(), 'data')
	}
});

const pathencode = s => s.replace(/[^A-Za-z0-9]/g, x => `_${('0000' +
	x.charCodeAt(0).toString(16)).substr(-4)}`);

const discesc = s => s.replace(/[^A-Za-z0-9\s]/g, m => '\\' + m);

const mkgetdir = abs => async () => {
	try {
		await util.promisify(fs.mkdir)(abs);
		return abs;
	} catch (e) {
		if(e.code !== 'EEXIST') throw e;
		return abs;
	}
};
const getdatadir = mkgetdir(config.datadir);
const getrepodir = mkgetdir(config.repodir);

const loadjson = async fn => {
	try {
		const raw = await util.promisify(fs.readFile)(fn);
		return JSON.parse(raw.toString());
	} catch (e) {
		if(e.code !== 'ENOENT') throw e;
	}
};

const sleep = ms => new Promise(r => setTimeout(r, ms));

let reqq;
const needlewrap = async (method, uri, ...args) => {
	const delay = 2000;
	for(let attempt = 1; attempt < 50; attempt++) {
		const requesting = (async () => {
			if(reqq) await reqq;
			console.log(`# ${uri}`);
			return await needle(method, uri, ...args);
		})();
		reqq = requesting.catch(() => {})
			.then(() => sleep(500));
		const resp = await requesting;
		if(/^2\d\d/.test(resp.statusCode.toString()))
			return resp;
		else if(resp.statusCode === 429) {
			console.warn(`${uri} HTTP 429 attempt ${attempt}`);
			await sleep(delay + Math.random() * delay);
		} else
			throw `${uri} HTTP ${resp.statusCode}`;
	}
};
const getapijson = async suff => {
	const resp = await needlewrap('get', `https://content.minetest.net/api/packages/${suff}`);
	return resp.body;
};

const syscmd = async (...args) => {
	const logcmd = args.join(' ');
	const cmd = args.shift();
	const proc = childproc.spawn(cmd, args, {
		stdio: ['ignore', 'pipe', 'inherit']
	});
	const spool = new Promise(res => {
		let buff = '';
		proc.stdout.on('data', x => buff += x.toString());
		proc.stdout.on('close', () => res(buff));
	});
	await new Promise((res, rej) => {
		proc.on('error', rej);
		proc.on('exit', (code, sig) => code ? rej(`${logcmd}: code ${code}`) :
			sig ? rej(`${logcmd}: signal ${sig}`) : res());
	});
	return await spool;
};

const getchangelog = async (reporoot, src, oldrel, newrel) => {
	const newhash = newrel && newrel.commit;
	if(!newhash)
		return '*no git commit for current release*';
	const oldhash = oldrel && oldrel.commit;
	if(oldrel && !oldhash)
		return '*previous release not from git*';

	const repodir = path.join(await reporoot, `repo-${pathencode(src)}.git`);
	const exist = await util.promisify(fs.exists)(repodir);
	if(!exist)
		await syscmd('git', 'clone', '--mirror', '--bare', src, repodir);
	else
		await syscmd('git', `--git-dir=${repodir}`, 'remote', 'update');

	let rawlog;
	try {
		rawlog = await syscmd('git', `--git-dir=${repodir}`, 'log',
			'--reverse', '--pretty=format:%s',
			oldrel ? `${oldhash}..${newhash}` : newhash);
	} catch (err) {
		return '*error comparing revisions*';
	}
	const seen = {};
	const parts = rawlog.replace(/`/g, '\U200B`')
		.split('\n')
		.map(x => x.trim())
		.filter(x => {
			if(!x || seen[x]) return;
			return (seen[x] = true);
		})
		.map(x => (x.length > 72) ? `${x.substr(0, 72)}...` : x);

	const calclen = seed => parts.reduce((a, b) => a + b.length + 1, seed);
	if(calclen(0) > config.maxlen) {
		let trunc = 0;
		const truncmsg = () => `[... snipped ${trunc} lines ...]`;
		parts.splice(0, 0, truncmsg());
		while(calclen(truncmsg()
				.length) > config.maxlen) {
			parts.shift();
			parts.shift();
			trunc++;
			parts.splice(0, 0, truncmsg());
		}
	}
	if(!parts.length)
		return '*empty changelog*';
	const codeblock = '```';
	return codeblock + parts.join('\n') + codeblock;
};

const procpkg = async (author, pkg) => {
	const storefile = path.join(await getdatadir(), `oldrel-${pathencode(author)
		}-${pathencode(pkg)}.json`);
	let oldrel = await loadjson(storefile);

	const reldata = await getapijson(`${author}/${pkg}/releases/`);
	const newrel = reldata && reldata.length && reldata[0];
	if(!newrel) throw 'no current release';
	oldrel = oldrel || reldata && (reldata.length > 1) && reldata[1];
	if(newrel && oldrel && newrel.commit === oldrel.commit) {
		console.warn(`no new release for ${author}/${pkg}`);
		return;
	}

	const pkgdata = await getapijson(`${author}/${pkg}/`);
	const changelog = pkgdata.repo ?
		(await getchangelog(await getrepodir(), pkgdata.repo, oldrel, newrel)) :
		'*no git repository*';

	const subst = {
		A: discesc(pkgdata.author),
		P: discesc(pkgdata.title),
		N: discesc(pkgdata.name),
		R: discesc(newrel.title),
		C: changelog,
		n: '\n'
	};
	subst['%'] = '%';
	const content = config.fmt.replace(/%(.)/g, m => subst[m[1]] || '');

	await needlewrap('post', config.hook, {
		content
	});

	await util.promisify(fs.writeFile)(storefile, JSON.stringify(newrel));
};

const procsearch = async term => {
	const results = await getapijson(`?q=${term}`);
	const errors = {};
	await Promise.all(results
		.filter(r => !config[`skip-${r.author.toLowerCase()}-${r.name.toLowerCase()}`])
		.map(r => procpkg(r.author, r.name)
			.catch(e => errors[`${r.author}/${r.name}`] = util.inspect(e))));
	if(Object.keys(errors)
		.length)
		throw JSON.stringify(errors, null, '\t');
};

if(!config.hook)
	throw 'missing required config --hook=...';
if(config.search)
	procsearch(config.search);
else if(config.author && config.pkg)
	procpkg(config.author, config.pkg);
else
	throw 'either (--search=...) or (--author=... --pkg=...) required';
